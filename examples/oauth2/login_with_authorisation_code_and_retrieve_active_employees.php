<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$config = [
    'client_id' => null,
    'crypt_key' => null,
    'client_secret' => null,
    'redirect_uri' => 'http://timeapi.local/examples/oauth2/login_with_authorisation_code_and_retrieve_active_employees.php',
    'response_type' => 'code',
    'scope' => 'read',
];
?>
<html>
    <head>
        <title>Log in with Binary City Time demo</title>
    </head>
    <body>
        <h1>Log in with Binary City Time demo</h1>
        <p>
            This demonstrates how to login with Binary City Time, using the grant type, authorisation_code. Then, use the retrieved code to retrieve an access token and use said access token to perform a query via the Binary City Time API.
        </p>
        <p>
            <b>PLEASE NOTE:</b> The coding structure used in this demonstration is terrible. Please do not use it as a frame of reference for proper coding structure in your production project(s). See it as a simple demonstration and use it for this purpose only.
        </p>
        <p>
            <?php
            if ( ! isset( $_GET['code'] ) ) {
                ?>
                <form>
                    <?php
                    $config_excludes = ['crypt_key', 'client_secret'];
                    foreach ( $config as $k => $v ) {
                        if ( ! in_array( $k, $config_excludes ) ) {
                            echo '<input type="hidden" name="', $k, '" value="', $v, '" />';
                        }
                    }
                    ?>
                    <input type="submit" value="Log in with Binary City Time" formaction="<?php echo URL_TIME_AUTH; ?>" />
                </form>
                <?php
            } else {
                $token_access = new TokenAccess( $config['client_id'], $config['crypt_key'], /* $grant_type: */OAuth2GrantType::auth_code, /* $code: */$_GET['code'] );
                $token_access->setClientSecret( $config['client_secret'] );
                $api = new Api( $token_access );
                echo '<h1>Success: Employee data retrieved via Binary City API, using grant type, authorisation_code</h1>';
                $page = 0;
                do {
                    if ( ! $api->get( $result, ContType::emp, /*$cont_uid = */Api::UID_GET_ALL, /*$data = */['filter_status' => UsrNEmpStatus::active], ++$page ) ) {
                        if ( 1 === $page ) {
                            echo '<pre>', var_export( $result, true ), '</pre>';
                        }
                        break;
                    }
                    echo 'Page: ', $page, '<br />',
                             '<pre>', var_export( $result, true ), '</pre>',
                             '<br /><br />';
                } while ( ApiReqStatus::success == $result['status'] );
            }
            ?>
        </p>
    </body>
</html>