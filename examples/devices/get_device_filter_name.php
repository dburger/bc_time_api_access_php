<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$page = 0;
$data = ['filter_name' => 'test'];
if ( ! $api->get( $result, ContType::device, /*$cont_uid = */Api::UID_GET_ALL, $data, ++$page ) ) {
    if ( 1 === $page ) {
        echo '<pre>', var_export( $result, true ), '</pre>';
    }
}
echo 'Page: ', $page, '<br />',
        '<pre>', var_export( $result, true ), '</pre>',
        '<br /><br />';