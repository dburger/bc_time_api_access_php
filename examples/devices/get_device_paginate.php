<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$page = 0;
do {
    if ( ! $api->get( $result, ContType::device, /*$cont_uid = */Api::UID_GET_ALL, /*$data = */null, ++$page ) ) {
        if ( 1 === $page ) {
            echo '<pre>', var_export( $result, true ), '</pre>';
        }
        break;
    }
    echo 'Page: ', $page, '<br />',
           '<pre>', var_export( $result, true ), '</pre>',
           '<br /><br />';
} while ( ApiReqStatus::success == $result['status'] );