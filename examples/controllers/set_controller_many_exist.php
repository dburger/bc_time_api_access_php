<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$data = [
    ['content_uid' => 4, 'attendance_last_communicated' => '2020-01-21 13:04:00'],
    ['content_uid' => 3, 'attendance_last_communicated' => '2020-01-21 13:06:00', 'last_check_in' => '2020-01-21 13:06:00'],
];
if ( ! $api->set( $result, ContType::controller, /*$cont_uid = */Api::UID_POST_MANY, $data ) ) {
    //handle failed request here
}
echo '<pre>', var_export( $result, true ), '</pre>';