<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$page = 0;
do {
    if ( ! $api->get( $result, ContType::controller, /*$cont_uid = */Api::UID_GET_ALL, /*$data = */['filter_status' => UsrNEmpStatus::active], ++$page ) ) {
        if ( 1 === $page ) {
            echo '<pre>', var_export( $result, true ), '</pre>';
        }
        break;
    }
    echo 'Page: ', $page, '<br />',
           '<pre>', var_export( $result, true ), '</pre>',
           '<br /><br />';
} while ( ApiReqStatus::success == $result['status'] );