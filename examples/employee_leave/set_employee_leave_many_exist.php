<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$data = [
    ['content_uid' => 908, 'leave_type' => LeaveType::off_day_paid, 'end' => '2018-12-05'],
    ['content_uid' => 909, 'start' => '2018-12-05']
];
if ( ! $api->set( $result, ContType::emp_leave, /*$cont_uid = */Api::UID_POST_MANY, $data ) ) {
    //handle failed request here
}
echo '<pre>', var_export( $result, true ), '</pre>';