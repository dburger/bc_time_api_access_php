<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$data = [
    'employee' => 5010,
    'leave_type' => LeaveType::awol_unpaid,
    'start' => '2018-12-04',
    'end' => '2018-12-05'
];
if ( ! $api->set( $result, ContType::emp_leave, /*$cont_uid = */null, $data ) ) {
    //handle failed request here
}
echo '<pre>', var_export( $result, true ), '</pre>';