<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$data = [
    ['employee' => 4597, 'leave_type' => LeaveType::annual_paid, 'start' => '2018-12-05', 'end' => '2018-12-06', 'manual_hour' => YesNo::yes, 'hour' => 5],
    ['employee' => 5010, 'leave_type' => LeaveType::compassion_paid, 'start' => '2018-12-05', 'end' => '2018-12-05']
];
if ( ! $api->set( $result, ContType::emp_leave, /*$cont_uid = */Api::UID_POST_MANY, $data ) ) {
    //handle failed request here
}
echo '<pre>', var_export( $result, true ), '</pre>';