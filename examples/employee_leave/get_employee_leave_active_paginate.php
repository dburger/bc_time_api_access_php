<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$page = 0;
$data = [
    'filter_start' => '2018-11-01', //compulsory
    'filter_end' => '2018-11-30', //compulsory
    'filter_status' => implode( ',', [UsrNEmpStatus::active, UsrNEmpStatus::inactive] ),
    'filter_leave_type' => implode( ',', [LeaveType::absent_appr_paid, LeaveType::compassion_paid] )
];
do {
    if ( ! $api->get( $result, ContType::emp_leave, /*$cont_uid = */Api::UID_GET_ALL, $data, ++$page ) ) {
        if ( 1 === $page ) {
            echo '<pre>', var_export( $result, true ), '</pre>';
        }
        break;
    }
    echo 'Page: ', $page, '<br />',
           '<pre>', var_export( $result, true ), '</pre>',
           '<br /><br />';
} while ( ApiReqStatus::success == $result['status'] );