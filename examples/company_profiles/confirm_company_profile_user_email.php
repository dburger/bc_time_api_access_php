<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$data = [
    'filter_user_status' => UsrNEmpStatus::active,
    'filter_user_name' => 'john@doe.com',
];
$api->get( $result, ContType::comp_prof_mem_usr_email_confirm, /*$cont_uid = */Api::UID_GET_ALL, $data );
echo '<pre>', var_export( $result, true ), '</pre>';