<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$data = [
    ['cont_uid' => 5010, 'name' => 'Darius'],
    ['cont_uid' => 4907, 'name' => 'Lena']
];
if ( ! $api->set( $result, ContType::emp, /*$cont_uid = */Api::UID_POST_MANY, $data ) ) {
    //handle failed request here
}
echo '<pre>', var_export( $result, true ), '</pre>';