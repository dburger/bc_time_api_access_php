<?php
require_once dirname( dirname( dirname( __FILE__ ) ) ) . '/gen/config.php';
$page = 0;
do {
    if ( ! $api->get( $result, ContType::visitor_sec_area_dur, /*$cont_uid = */Api::UID_GET_ALL, /*$data = */['filter_period_start' => '2017-08-17 00:00:00', 'filter_period_end' => '2017-08-23 23:59:59'], ++$page ) ) {
        if ( 1 === $page ) {
            echo '<pre>', var_export( $result, true ), '</pre>';
        }
        break;
    }
    echo 'Page: ', $page, '<br />',
           '<pre>', var_export( $result, true ), '</pre>',
           '<br /><br />';
} while ( ApiReqStatus::success == $result['status'] );