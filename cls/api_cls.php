<?php
class Api {
    const UID_GET_ALL = -1;
    const UID_POST_MANY = -2;
    const ROW_COUNT = 100; //100 is the maximum no. of rows the API allows to retrieved at a time

    private $g_token_access = null,
                $g_crypt = null;

    private function getTokenAccess() {
        if ( ! isset( $this->g_token_access ) ) {
            $this->g_token_access = new TokenAccess();
        }
        return $this->g_token_access;
    }

    public function setTokenAccess( $value ) {
        if ( is_object( $value ) ) {
            $this->g_token_access = $value;
        }
        return $this;
    }

    private function getCrypt() {
        if ( ! isset( $this->g_crypt ) ) {
            $this->g_crypt = new Crypt();
        }
        return $this->g_crypt;
    }

    public function setCrypt( $value ) {
        if ( is_object( $value ) ) {
            $this->g_crypt = $value;
        }
        return $this;
    }

    public function __construct( $token_access ) {
        $this->setTokenAccess( $token_access );
        if ( $this->getTokenAccess()->getCryptKey( /*$valid = */true ) ) {
            $this->setCrypt( $token_access->getCrypt() );
        }
    }

    public function get( &$result, $cont_type_id, $cont_uid = null, $data = null, $page = 1, $row_count = self::ROW_COUNT, $try = 0 ) {
        isset( $result ) && $result = null;
        if ( ! $this->getTokenAccess()->deterToken() ) {
            return false;
        } else if ( ! $this->deterDataGet( $req_data, $cont_type_id, $cont_uid, $data, $page, $row_count ) ) {
            return false;
        } else if ( ! Conv::deterResp( $result, $req_data, URL_TIME_API ) ) {
            return false;
        }
        if ( isset( $this->g_crypt )
                && ! $this->getCrypt()->setData( $result )->deterDecrypt( $result ) ) {
            return false;
        }
        $result = json_decode( $result, true );
        if ( ! isset( $result['status'] ) ) {
            return false;
        } else if ( ApiReqStatus::token_access_expired == $result['status'] ) {
            if ( 1 === ++$try ) {
                return $this->get( $result, $cont_type_id, $cont_uid, $data, $page, $row_count, $try ); //recursion, but only once
            }
            return false;
        }
        return ( ApiReqStatus::success == $result['status'] );
    }

    private function deterDataGet( &$req_data, $cont_type_id, $cont_uid, $data = null, $page = 1, $row_count = self::ROW_COUNT ) {
        $req_data = ['access_token' => $this->getTokenAccess()->getToken()];
        $_data = [
            'cont_type_id' => $cont_type_id,
            'cont_uid' => ( ! is_array( $cont_uid ) ? $cont_uid : implode( ',', $cont_uid ) )
        ];
        if ( Valid::isNum( $page, /*$min = */1 ) && Valid::isNum( $row_count, /*$min = */1, /*$max = */self::ROW_COUNT ) ) { //paginate
            $_data['page'] = $page;
            $_data['row_count'] = $row_count;
        }
        if ( ! empty( $data ) && is_array( $data ) ) {
            foreach ( $data as $k => $v ) {
                $_data[$k] = $v;
            }
        }
        if ( ! isset( $this->g_crypt ) ) {
            $req_data = http_build_query( array_merge( $req_data, $_data ) );
            return true;
        }
        if ( $this->getCrypt()->setData( json_encode( $_data ) )->deterEncrypt( $_data ) ) {
            $req_data = http_build_query( array_merge( $req_data, ['data' => $_data] ) );
            return true;
        }
        return false;
    }

    public function set( &$result, $cont_type_id, $cont_uid, $data, $try = 0 ) {
        isset( $result ) && $result = null;
        if ( ! $this->getTokenAccess()->deterToken() ) {
            return false;
        } else if ( ! $this->deterDataSet( $data, $cont_type_id, $cont_uid, $data ) ) {
            return false;
        } else if ( ! Conv::deterResp( $result, $data, URL_TIME_API, /*$post = */true ) ) {
            return false;
        }
        if ( isset( $this->g_crypt )
                && ! $this->getCrypt()->setData( $result )->deterDecrypt( $result ) ) {
            return false;
        }
        $result = json_decode( $result, true );
        if ( ! isset( $result['status'] ) ) {
            return false;
        } else if ( ApiReqStatus::token_access_expired == $result['status'] ) {
            if ( 0 === ++$try ) {
                return $this->set( $result, $cont_type_id, $cont_uid, $data, $try ); //recursion, but only once
            }
            return false;
        }
        return ( ApiReqStatus::success == $result['status'] );
    }

    private function deterDataSet( &$req_data, $cont_type_id, $cont_uid, $data ) {
        $req_data = ['access_token' => $this->getTokenAccess()->getToken()];
        $_data = ['cont_type_id' => $cont_type_id];
        if ( ! empty( $cont_uid ) ) { //empty/omitted will result in new object
            $_data['cont_uid'] = $cont_uid;
        }
        if ( ! empty( $data ) && is_array( $data ) ) {
            if ( self::UID_POST_MANY != $cont_uid ) {
                foreach ( $data as $k => $v ) {
                    $_data[$k] = $v;
                }
            } else {
                $_data['data'] = $data;
            }
        }
        if ( ! isset( $this->g_crypt ) ) {
            $req_data = http_build_query( array_merge( $req_data, $_data ) );
            return true;
        }
        if ( $this->getCrypt()->setData( json_encode( $_data ) )->deterEncrypt( $_data ) ) {
            $req_data = http_build_query( array_merge( $req_data, ['data' => $_data] ) );
            return true;
        }
        return false;
    }
}