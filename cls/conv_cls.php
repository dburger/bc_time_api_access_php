<?php
class Conv {
    public static function deterAutoloadPath( $in, &$out ) {
        '' !== $out && $out = '';
        $len = strlen( $in );
        $in_lower = strtolower( $in );
        $chars = array_flip( range( 'A', 'Z' ) );
        for ( $i = 0 ; $i < $len ; $i++ ) {
            if ( isset( $chars[$in[$i]] ) && '' !== $out ) {
                 $out .= '_';
            }
            $out .= $in_lower[$i];
        }
        return ( '' !== $out );
    }

    public static function deterResp( &$result, $data, $url, $post = false, $try = 0, $try_max = 3 ) {
        isset( $result ) && $result = null;
        if ( ! $post ) { //handle get request
            $result = file_get_contents( "{$url}?{$data}" ); //so much faster than curl
            if ( ! empty( $result ) ) {
                return true;
            }
            return (
                ++$try <= $try_max
                ? self::deterResp( $result, $data, $url, $post, $try, $try_max ) //recursion
                : false
            );
        }
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTPHEADER, [
            'Host: ' . HOST_TIME,
            'Content-Type: application/x-www-form-urlencoded',
            'Content-Length: ' . strlen( $data )
        ]);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec( $ch );
        curl_close( $ch );
        if ( ! empty( $result ) ) {
            return true;
        }
        return (
            ++$try <= $try_max
            ? self::deterResp( $result, $data, $url, $post, $try, $try_max ) //recursion
            : false
        );
    }

    public static function deterCryptRandCode( &$value, $len, $strict = false, $rm_common = true ) {
        '' !== $value && $value = '';
        if ( ! Valid::isNum( $len, /*$min = */1 ) ) {
            return false;
        } else if ( ! function_exists( 'openssl_random_pseudo_bytes' ) ) {
            return false;
        }
        $value = base64_encode( openssl_random_pseudo_bytes( $len ) );
        if ( $rm_common ) {
            $value = str_replace( ['/', '+', '='], '', $value );
        }
        if ( ! $strict ) {
            return ( '' !== $value );
        }
        $value = substr( $value, 0, $len );
        return Valid::isLen( $value, $len );
    }

    /* e.g. if $range = 3000 (bin: 101110111000)
     * +--------+--------+
     *  |....1011|10111000|
     *  +--------+--------+
     *  bit = 12, byte = 2, bit_max = 2^12 = 4096
     */
    public static function deterCryptRandNum( &$value, $min, $max, $max_rep = 2 ) {
        isset( $value ) && $value = null;
        if ( ! Valid::isNum( [$min, $max], /*$min = */1 ) ) {
            return false;
        } else if ( ( $diff = ( $max - $min ) ) <= 0 ) {
            return false;
        }
        $range = ( $diff + 1 ); // because $max is inclusive
        $bit = ceil( log( $range, 2 ) );
        $byte = ceil( $bit / 8.0 );
        $bit_max = ( 1 << $bit );
        do {
            $num = hexdec( bin2hex( openssl_random_pseudo_bytes( $byte ) ) ) % $bit_max;
            if ( $num >= $range ) {
                continue;
            }
            if ( Valid::isLen( ( string )$max, /*$len = */10, /*$strict = */false ) && Valid::isNum( $max_rep, /*$min = */1 ) ) {
                $value_str = ( string )( $num + $min );
                $len = strlen( $value_str );
                $values = [];
                for ( $i = 0 ; $i < $len ; $i++ ) {
                    $values[] = substr( $value_str, $i, 1 );
                }
                $values = array_filter( array_count_values( $values ), function( $a ) use ( $max_rep ) {
                    return ( $a > $max_rep );
                });
                if ( ! empty( $values ) ) {
                    continue;
                }
            }
            break;
        } while ( true );
        return Valid::isBetween( $value = ( $num + $min ), $min, $max );
    }
}