<?php
class TokenAccess {
    private $g_grant_type = OAuth2GrantType::client_credential,
              $g_client_id = null,
              $g_client_secret = null,
              $g_code = null,
              $g_crypt_key = null,
              $g_key_private_file_path = null,
              $g_crypt = null,
              $g_token = null,
              $g_token_expire = null;

    public function setGrantType( $value ) {
        if ( OAuth2GrantType::exists( $value ) ) {
            $this->g_grant_type = $value;
        }
        return $this;
    }

    public function setClientId( $value ) {
        if ( ! empty( $value ) ) {
            $this->g_client_id = $value;
        }
        return $this;
    }

    private function getClientSecret() {
        if ( ! isset( $this->g_crypt_key ) ) {
            return $this->g_client_secret;
        }
        if ( $this->getCrypt()->setData( $this->g_client_secret )->deterEncrypt( $value ) ) {
            return $value;
        }
        return null;
    }

    public function setClientSecret( $value ) {
        if ( ! empty( $value ) ) {
            $this->g_client_secret = $value;
        }
        return $this;
    }

    public function setCode( $value ) {
        if ( ! empty( $value ) ) {
            $this->g_code = $value;
        }
        return $this;
    }

    public function getCryptKey( $valid = false ) {
        return ( ! $valid ? $this->g_crypt_key : isset( $this->g_crypt_key ) );
    }

    public function setCryptKey( $value ) {
        if ( Valid::isLen( $value, /*$len = */Crypt::LEN_KEY ) ) {
            $this->g_crypt_key = $value;
        }
        return $this;
    }

    public function setKeyPrivateFilePath( $value ) {
        if ( ! empty( $value ) && file_exists( $value ) ) {
            $this->g_key_private_file_path = $value;
        }
        return $this;
    }

    public function getCrypt() {
        if ( ! isset( $this->g_crypt ) ) {
            $this->g_crypt = new Crypt( /*$key = */$this->g_crypt_key );
        }
        return $this->g_crypt;
    }

    public function setCrypt( $value ) {
        if ( is_object( $value ) ) {
            $this->g_crypt = $value;
        }
        return $this;
    }

    public function getToken() {
        return $this->g_token;
    }

    public function getTokenExpire() {
        return $this->g_token_expire;
    }

    public function __construct( $client_id = null, $crypt_key = null, $grant_type = null, $code = null ) {
        isset( $client_id ) && $this->setClientId( $client_id );
        isset( $crypt_key ) && $this->setCryptKey( $crypt_key );
        isset( $grant_type ) && $this->setGrantType( $grant_type );
        isset( $code ) && $this->setCode( $code );
    }

    public function deterToken() {
        isset( $result ) && $result = null;
        $now = new DateTime();
        if ( isset( $this->g_token, $this->g_token_expire )
                && ! empty( $this->g_token )
                && $this->g_token_expire > $now ) {
            return true;
        }
        if ( ! $this->valid() ) {
            return false;
        } else if ( ! $this->deterData( $data ) ) {
            return false;
        } else if ( ! Conv::deterResp( $result, $data, URL_TIME_OAUTH2_TOKEN, /*$post = */true ) ) {
            return false;
        } else if ( isset( $this->g_crypt_key )
                && OAuth2GrantType::jwt_bearer !== $this->g_grant_type
                && ! $this->getCrypt()->setData( $result )->deterDecrypt( $result ) ) {
            return false;
        }
        $result = json_decode( $result, true );
        if ( ! isset( $result['status'] )
                || ApiReqStatus::success != $result['status'] ) {
            return false;
        }
        $this->g_token = $result['access_token'];
        $this->g_token_expire = ( new DateTime() )->modify( "+{$result['expires_in']} seconds" );
        unset( $result );
        return ( ! empty( $this->g_token ) ? $this->g_token_expire > $now : false );
    }

    private function valid() {
        switch ( $this->g_grant_type ) {
            case OAuth2GrantType::auth_code:
                return isset( $this->g_client_id, $this->g_client_secret, $this->g_code );
            case OAuth2GrantType::client_credential:
                return isset( $this->g_client_id, $this->g_client_secret );
            case OAuth2GrantType::jwt_bearer:
                return isset( $this->g_client_id, $this->g_key_private_file_path );
        }
        return false;
    }

    private function deterData( &$value ) {
        isset( $value ) && $value = null;
        switch ( $this->g_grant_type ) {
            case OAuth2GrantType::auth_code:
                $value = http_build_query([
                    'grant_type' => $this->g_grant_type,
                    'client_id' => $this->g_client_id,
                    'client_secret' => $this->getClientSecret(),
                    'code' => $this->g_code,
                ]);
                return true;
            case OAuth2GrantType::client_credential:
                $value = http_build_query([
                    'grant_type' => $this->g_grant_type,
                    'client_id' => $this->g_client_id,
                    'client_secret' => $this->getClientSecret(),
                ]);
                return true;
            case OAuth2GrantType::jwt_bearer:
                if ( ! $this->deterJwtAssertion( $assertion ) ) {
                    return false;
                }
                $value = http_build_query([
                    'grant_type' => $this->g_grant_type,
                    'assertion' => $assertion,
                ]);
                return true;
        }
        return false;
    }

    public function deterJwtAssertion( &$value, $algo = OPENSSL_ALGO_SHA1 ) {
        isset( $value ) && $value = null;
        $hd['algo'] = $algo;
        $data = [
            'client_id' => $this->g_client_id,
            'issued' => ( new DateTime( 'now', ( $tz = new DateTimeZone( 'UTC' ) ) ) )->format( 'Y-m-d H:i:s' ), //use UTC
            'expire' => ( new DateTime( 'now', $tz ) )->modify( '+60 seconds' )->format( 'Y-m-d H:i:s' ) //use UTC
        ];
        $values = [$hd, $data];
        foreach ( $values as &$v ) {
            $v = base64_encode( json_encode( $v ) );
        }
        $data_sign = implode( '.', $values );
        $key = file_get_contents( $this->g_key_private_file_path );
        if ( empty( $key ) ) {
            return false;
        } else if ( ! openssl_sign( $data_sign, $signature, $key, $algo ) ) {
            return false;
        }
        $values[] = base64_encode( $signature );
        $value = implode( '.', $values );
        return ( ! empty( $value ) );
    }
}