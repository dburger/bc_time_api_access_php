<?php
class Crypt {
    const IV_START_VALUE = 'a,b,c,d,e,f,g,h,A,B,C,D,E,F,G,H'; //a = 1, b = 2..., A = 9, B = 10..., and etc.
    const LEN_KEY = 32;
    const LEN_IV = 16;

    private $g_method = 'AES-256-CBC',
              $g_key = null, //secret key to encrypt/decrypt
              $g_data = null;  //data to encrypt/decrypt

    public function setMethod( $value ) {
        if ( ! empty( $value ) ) {
            if ( in_array( $value, openssl_get_cipher_methods() ) ) {
                $this->g_method = $value;
            }
        }
        return $this;
    }

    public function setKey( $value ) {
        ! empty( $value ) && $this->g_key = $value;
        return $this;
    }

    public function setData( $value ) {
        ! empty( $value ) && $this->g_data = $value;
        return $this;
    }

    public function __construct( $key = null, $data = null ) {
        isset( $key ) && $this->g_key = $key;
        isset( $data ) && $this->g_data = $data;
    }

    public function deterEncrypt( &$value ) {
        isset( $value ) && $value = null;
        if ( ! $this->deterIvStartEnd( $iv, $start, $end ) ) {
            return false;
        }
        $value = openssl_encrypt( $this->g_data, $this->g_method, $this->g_key, OPENSSL_RAW_DATA || OPENSSL_ZERO_PADDING, $iv );
        if ( false !== $value ) {
            $value = base64_encode( $value );
            $count = 0;
            while ( '=' === substr( $value, ( $len = ( strlen( $value ) - 1 ) ) ) ) {
                $value = substr( $value, 0, $len );
                ++$count;
            }
            $value = $start . $value . $end;
            0 !== $count && $value .= str_repeat( '=', $count );
            return true;
        }
        return false;
    }

    public function deterDecrypt( &$value ) {
        isset( $value ) && $value = null;
        if ( ! $this->extractIv( $this->g_data, $iv ) ) {
            return false;
        }
        $value = openssl_decrypt( base64_decode( $this->g_data ), $this->g_method, $this->g_key, OPENSSL_RAW_DATA || OPENSSL_ZERO_PADDING, $iv );
        return ( false !== $value );
    }

    private function deterIvStartEnd( &$iv, &$start, &$end ) {
        isset( $iv ) && $iv = null;
        isset( $start ) && $start = null;
        isset( $end ) && $end = null;
        if ( ! Conv::deterCryptRandCode( $iv, self::LEN_IV, true ) ) {
            return false;
        } else if ( ! Conv::deterCryptRandNum( $len_start, 1, self::LEN_IV ) ) {
            return false;
        }
        $values = explode( ',', self::IV_START_VALUE );
        if ( ! isset( $values[$len_start - 1] ) ) {
            return false;
        }
        $start = $values[$len_start - 1] . substr( $iv, 0, $len_start );
        $end = substr( $iv, $len_start );
        return Valid::isLen( $start . $end, self::LEN_IV + 1 );
    }

    private function extractIv( &$encrypted, &$value ) {
        isset( $value ) && $value = null;
        if ( ! isset( $encrypted[self::LEN_IV + 1] ) ) {
            return false;
        } else if ( ! $this->deterExtractIvLenStart( $encrypted, $len_start ) ) {
            return false;
        }
        $len_end = ( self::LEN_IV - $len_start );
        $count = 0;
        while ( '=' === substr( $encrypted, ( $len = ( strlen( $encrypted ) - 1 ) ) ) ) {
            $encrypted = substr( $encrypted, 0, $len );
            ++$count;
        }
        $value = substr( $encrypted, 0, $len_start ); //start
        $encrypted = substr( $encrypted, $len_start );
        $len_encrypted = strlen( $encrypted ); //end
        $value .= substr( $encrypted, $len_encrypted - $len_end );
        $encrypted = substr( $encrypted, 0, $len_encrypted - $len_end );
        if ( 0 !== $count ) {
            $encrypted .= str_repeat( '=', $count );
        }
        return Valid::isLen( $value, self::LEN_IV );
    }

    private function deterExtractIvLenStart( &$encrypted, &$len ) {
        isset( $len ) && $len = null;
        if ( empty( $encrypted ) ) {
            return false;
        }
        $values = explode( ',', self::IV_START_VALUE );
        if ( false === ( $k = array_search( substr( $encrypted, 0, 1 ), $values ) ) ) {
            return false;
        }
        $encrypted = substr( $encrypted, 1 );
        $len = ( $k + 1 );
        return Valid::isBetween( $len, 1, self::LEN_IV ); //zero based
    }
}