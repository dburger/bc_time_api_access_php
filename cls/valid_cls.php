<?php
class Valid {
    public static function isNum( $value, $min = null, $max = null ) {
        ! is_array( $value ) && $value = [$value];
        foreach ( $value as $v ) {
            if ( ! is_numeric( $v ) ) {
                return false;
            }
            switch ( true ) {
                case self::isNum( [$min, $max] ): //not recursion
                    if ( ! self::isBetween( $v, $min, $max ) ) {
                        return false;
                    }
                    break;
                case is_numeric( $min ):
                    if ( $v < $min ) {
                        return false;
                    }
                    break;
                case is_numeric( $max ):
                    if ( $v > $max ) {
                        return false;
                    }
                    break;
            }
        }
        return true;
    }

    public static function isBetween( $value, $min, $max, $incl = true ) {
        if ( ! self::isNum( [$value, $min, $max] ) ) {
            return false;
        }
        return (
            $incl
            ? ( $value >= $min && $value <= $max )
            : ( $value > $min && $value < $max )
        );
    }

    public static function isLen( $value, $len, $strict = true ) {
        ! is_array( $value ) && $value = [$value];
        foreach ( $value as $v ) {
            if ( $strict ) {
                if ( ! isset( $v[$len - 1] ) || isset( $v[$len] ) ) {
                    return false;
                }
            } else if ( isset( $v[$len] ) ) {
                return false;
            }
        }
        return true;
    }
}