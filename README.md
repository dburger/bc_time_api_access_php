# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

PHP implementation of using Binary City Time's API - https://www.bcity.me.

### How do I get set up? ###

* Summary of set up - download source code, update credentials and work through directory, "examples".
* Configuration - all configuration is done in "gen/config.php".
* Dependencies - OpenSSL
* How to run tests - work through directory, "examples".

### Who do I talk to? ###

* https://www.bcity.me/contact-us