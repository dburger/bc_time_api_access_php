<?php
final class ApiReqStatus {
    const none = 0;
    const success = 1;
    const data_invalid = 2;
    const insufficient_access = 3;
    const err = 4;
    const client_id_invalid = 5;
    const client_secret_invalid = 6;
    const method_invalid = 7;
    const cont_type_id_invalid = 8;
    const uid_invalid = 9;
    const field_invalid = 10;
    const usr_uid_invalid = 11;
    const post_batch_data_invalid = 12;
    const data_empty = 13;
    const resp_not = 14; //no internet connection, blocked by firewall, etc.; used exclusively by Time Comm.
    const resp_invalid = 15; //http code is anything other than 200 (OK); used exclusively by Time Comm.
    const resp_json_invalid = 16; //used exclusively by Time Comm.
    const comp_prof_active_not = 17;
    const cost_plan_overdue = 18;
    const token_access_invalid = 19;
    const token_access_expired = 20;
    const auth_credential_invalid = 21; //raised by Time Comm. Service
}

final class OAuth2GrantType {
    const auth_code = 'authorisation_code';
    const implicit = 'implicit';
    const usr_credential = 'password';
    const client_credential = 'client_credentials';
    const token_refresh = 'refresh_token';
    const jwt_bearer = 'urn:ietf:params:oauth:grant-type:jwt-bearer';

    public static function exists( $value ) {
        return in_array( $value, ( new ReflectionClass( __CLASS__ ) )->getConstants() );
    }
}

final class ContType {
    const usr = 10;
    const device = 18;
    const emp = 22;
    const branch = 35;
    const emp_leave = 50;
    const set = 86;
    const emp_otr_det = 103;
    const grp_emp_mem = 105;
    const dept = 39;
    const visitor = 95;
    const grp_visitor = 99;
    const sec_det = 250;
    const sec_area_dur = 252; //how much time spent in area(s)
    const visitor_sec_det = 350;
    const visitor_sec_area_dur = 352; //how much time spent in area(s)
    const controller = 111;
    const comp_prof_mem_usr = 114;
    const comp_prof_mem_usr_email_confirm = 115;
    const grp_visitor_mem = 116;
    const grp_visitor_area_mem = 117;
    const comp_prof_curr = 118;

    public static function exists( $value ) {
        return (
            is_numeric( $value )
            ? in_array( $value, ( new ReflectionClass( __CLASS__ ) )->getConstants() )
            : false
        );
    }
}

final class UsrNEmpStatus {
    const active = 1;
    const inactive = 2;
}

final class LeaveType {
    const annual_paid = 1;
    const sick_paid = 2;
    const sick_unpaid = 3;
    const compassion_paid = 4;
    const compassion_unpaid = 5;
    const study_paid = 6;
    const sport_paid = 7;
    const maternity_paid = 8;
    const paternity_paid = 9;
    const off_day_paid = 10;
    const off_day_unpaid = 11;
    const awol_unpaid = 12;
    const unpaid = 13;
    const medical_exam_paid = 14;
    const train_paid = 15;
    const absent_appr_paid = 16;
    const target_reached_paid = 17;
    const maternity_unpaid = 18;
    const suspend_paid = 19;
    const suspend_unpaid = 20;
}

final class YesNo {
    const yes = 1;
    const no = 2;
}