<?php
$root = dirname( dirname( __FILE__ ) );
require_once( "{$root}/cls/conv_cls.php" );
require_once( 'enum.php' );
spl_autoload_register( function( $cls_name ) use ( $root ) {
    $dir = "{$root}/cls";
    $cls_path = '/';
    $values = explode( '\\', $cls_name );
    if ( 0 !== ( $count = count( $values ) ) ) {
        for ( $i = 0 ; $i < ( $count - 1 ) ; $i++ ) {
            if ( Conv::deterAutoloadPath( $values[$i], $value ) ) {
                $cls_path .= "{$value}/";
            }
        }
    }
    $cls_name = $values[$count - 1];
    unset( $values );
    if ( Conv::deterAutoloadPath( $cls_name, $value ) ) {
        $cls_path .= $value;
        if ( file_exists( $path = "{$dir}{$cls_path}_cls.php" ) ) {
            require_once( $path );
        }
    }
});

define( 'HOST_TIME', 'time.bcity.me' );
define( 'DOMAIN_TIME', 'https://time.bcity.me' );
define( 'URL_TIME_OAUTH2_TOKEN', DOMAIN_TIME . '/oauth2/token/' );
define( 'URL_TIME_API', DOMAIN_TIME . '/api/' );
define( 'URL_TIME_AUTH', DOMAIN_TIME . '/oauth2/authorise/' );

/*** START - Using grant type, Client Credentials ***/
$token_access = new TokenAccess(
    /*$client_id = */'{your client ID}',
    /*$crypt_key = */'{your encryption key}', //omit or pass null if end-to-end encryption is not used
    /*$grant_type = */OAuth2GrantType::client_credential
);
$token_access->setClientSecret( '{your client secret}' );
/*** END - Using grant type, Client Credentials ***/

/*** START - Using grant type, JWT Bearer ***/
// $token_access = new TokenAccess(
//     /*$client_id = */'{your client ID}',
//     /*$crypt_key = */'{your encryption key}', //omit or pass null if end-to-end encryption is not used
//     /*$grant_type = */OAuth2GrantType::jwt_bearer
// );
/*
 * Using OpenSSL, follow these to steps to generate a private & public key par
 * openssl genrsa -out privatekey.pem 1024
 * openssl req -new -x509 -key privatekey.pem -out publickey.cer -days 1825
 */
// $token_access->setKeyPrivateFilePath( "{$root}/_files/privatekey.pem" );
/*** END - Using grant type, JWT Bearer ***/

$api = new Api( $token_access );